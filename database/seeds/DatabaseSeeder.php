<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * run the database seeds
     *
     * @return void
     */
    public function run(){
        $this->call(PruebaSeeder::class);
    }
}